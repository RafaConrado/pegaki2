const stream = require("stream");

class CsvParser extends stream.Transform {
  constructor() {
    super({ objectMode: true });
  }

  _transform(chunk, enc, done) {
    const formattedChunk = chunk.toString(enc);
    const lines = formattedChunk.split("\n");
    const values = lines.map((line) => line.split(","));

    for (const item of values) {
      this.push(item);
    }

    done();
  }
}

module.exports = CsvParser;
