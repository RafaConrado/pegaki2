const stream = require("stream");

class NearbyTransformer extends stream.Transform {
  mapsApi = null;
  placeType = "store";

  constructor(mapsApi, placeType) {
    super({ objectMode: true });
    if (!mapsApi) throw new Error("mapsApi is required");
    this.mapsApi = mapsApi;
    if (placeType) this.placeType = placeType;
  }

  async _transform(chunk, enc, done) {
    const cep = chunk.toString();
    if (!cep) this.push("");
    console.log("searching cep: ", cep);
    const location = await this.mapsApi.getLocationFromCep(cep.toString());
    if (!location.candidates.length) done();
    try {
      const geo = location.candidates[0].geometry.location;
      const places = await this.mapsApi.getPlacesFromLocation(
        geo.lat,
        geo.lng,
        this.placeType
      );
      this.push(
        places.results.map((place) => ({
          name: place.name,
          address: place.vicinity,
        }))
      );
      done();
    } catch (e) {
      console.log(e.message);
    }
  }
}

module.exports = NearbyTransformer;
