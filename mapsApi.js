const fetch = require("node-fetch");

class MapsApi {
  key = "";

  constructor(key) {
    this.key = key;
  }

  async getLocationFromCep(cep) {
    const result = await fetch(
      `https://maps.googleapis.com/maps/api/place/findplacefromtext/json?key=${this.key}&input=${cep}&inputtype=textquery&fields=geometry`
    );

    return result.json();
  }

  async getPlacesFromLocation(lat, lng, placeType, radius = 1000) {
    const result = await fetch(
      `https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=${this.key}&location=${lat},${lng}&radius=${radius}&fields=formatted_address,name,geometry&type=${placeType}`
    );

    return result.json();
  }
}

module.exports = MapsApi;
