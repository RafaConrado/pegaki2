const stream = require("stream");

class CsvWriter extends stream.Transform {
  constructor() {
    super({ objectMode: true });
  }

  _transform(chunk, enc, done) {
    for (const item of chunk) {
      const values = [];
      for (const value of Object.values(item)) {
        values.push(value);
      }
      this.push(values.join(",") + "\n");
    }

    done();
  }
}

module.exports = CsvWriter;
