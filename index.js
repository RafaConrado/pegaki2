const MapsApi = require("./mapsApi");
const NearbyTransformer = require("./nearbyTransformer");
const CsvParser = require("./csvParser");
const CsvWriter = require("./csvWriter");
const fs = require("fs");
const resultFileName = "result.csv";
const write = fs.createWriteStream(resultFileName);

const arguments = process.argv;

if (arguments.length !== 4) {
  console.error(arguments);
  throw new Error("path, and place type are required");
}

const csvPath = arguments[2];
const placeType = arguments[3];
const apiKey = process.env.PLACES_API_KEY;

const mapsApi = new MapsApi(apiKey);
const nearbyTransformer = new NearbyTransformer(mapsApi, placeType);

fs.createReadStream(csvPath)
  .pipe(new CsvParser())
  .pipe(nearbyTransformer)
  .pipe(new CsvWriter())
  .pipe(write);
