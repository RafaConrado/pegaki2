# PEGAKI 2

## Google API client key

Para testar essa aplicação você precisará de uma variável de ambiente contendo sua api key com nome de ```PLACES_API_KEY```, você execute:

```shell
export PLACES_API_KEY=MY_GOOGLE_API_KEY
```

## Executando

Para executar este serviço você deve passar 2 parâmetros para execução, ```path``` para o csv e ```type``` para os *places* que serão buscados na api do google:

```shell
node index.js ceps.csv supermarket
```

Por padrão este serviço já contém um arquivo CSV de exemplo na raiz do projeto chamado ```ceps.csv```, o resultado da execução deste serviço será salvo em um arquivo chamado ```result.csv``` na raiz deste projeto, para isso verifique as permissões da pasta caso use ambiente Windows.